The LibreOrganize Book
=======================

.. image:: _static/libreorganize.png
    :alt: LibreOrganize Logo

a Web Platform for Local Unions and Progressive Organizations
---------------------------------------------------------------

Last updated: 15 July 2019

* `Copyright Notice <copyright.html>`_
* `Chapter 1 <ch01.html>`_ *Membership Management*
* `Chapter 2 <ch02.html>`_ *Meeting Attendance*
* `Chapter 3 <ch03.html>`_ *Online Discussion Forms*
* `Chapter 4 <ch04.html>`_ *Anonymous Online Voting* 
* `GNU Free Document License <fdl-1.3.html>`_ 
  
.. toctree::
    :maxdepth: 1
    :hidden:

    copyright.rst

.. toctree::
    :maxdepth: 1
    :numbered:
    :hidden:

    ch01.rst
    ch02.rst
    ch03.rst
    ch04.rst

.. toctree::
    :maxdepth: 1
    :hidden:

    fdl-1.3.rst

.. toctree::
    :maxdepth: 1
    :hidden:
    :glob:

* :ref:`search`
